package dev.vrba.cartographers.server.service

import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.MongoDBContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers

@Testcontainers
@SpringBootTest
class PlayerServiceTests {

    companion object {
        @Container
        val mongo = MongoDBContainer("mongo:6.0")

        @JvmStatic
        @DynamicPropertySource
        fun configure(registry: DynamicPropertyRegistry) {
            registry.add("spring.data.mongodb.uri", mongo::getReplicaSetUrl)
        }
    }

    @Autowired
    private lateinit var service: PlayerService


    @Test
    fun `test that each player is assigned an unique token`() {
        runBlocking {
            val first = service.createPlayer("Jirka")
            val second = service.createPlayer("Eliška")

            assertEquals("Jirka", first.username)
            assertEquals("Eliška", second.username)

            assertNotNull(first.token)
            assertNotNull(second.token)

            assertEquals(32, first.token.length)
            assertEquals(32, second.token.length)

            assertNotEquals(first.token, second.token)
        }
    }

    @Test
    fun `test that player can be retrieved by a token`() {
        runBlocking {
            val player = service.createPlayer("Jirka")

            assertNotNull(player.token)
            assertEquals(32, player.token.length)

            val found = service.findPlayerByToken(player.token)

            assertNotNull(found)
            assertEquals(player.id, found?.id)
            assertEquals(player.username, found?.username)
            assertEquals(player.token, found?.token)
        }
    }
}