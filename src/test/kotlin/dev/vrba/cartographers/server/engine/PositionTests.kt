package dev.vrba.cartographers.server.engine

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow

class PositionTests {

    @Test
    fun `test position can be created`() {
        assertDoesNotThrow {
            Position(1, 1)
            Position(-1, -1)
            Position(1, -1)
            Position(-1, 1)
            Position(100000, 100000)
            Position(-100000, 100000)
            Position(-100000, -100000)
        }
    }

    @Test
    fun `test positions can be added together`() {
        val origin = Position(10, 23)
        val result = origin + Position(3, -2)

        assertEquals(10, origin.x)
        assertEquals(23, origin.y)
        assertEquals(13, result.x)
        assertEquals(21, result.y)
    }

    @Test
    fun `test position returns its neighbour positions correctly`() {
        val origin = Position(30, -20)
        val neighbours = origin.neighbours()

        assertEquals(8, neighbours.size)
        assertTrue(neighbours.contains(Position(29, -21)))
        assertTrue(neighbours.contains(Position(29, -20)))
        assertTrue(neighbours.contains(Position(29, -19)))
        assertTrue(neighbours.contains(Position(30, -21)))
        assertTrue(neighbours.contains(Position(30, -19)))
        assertTrue(neighbours.contains(Position(31, -21)))
        assertTrue(neighbours.contains(Position(31, -20)))
        assertTrue(neighbours.contains(Position(31, -19)))
    }
}