package dev.vrba.cartographers.server.engine

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class TileTests {

    @Test
    fun `test tiles correctly reports empty state`() {
        assertTrue(Tile(Position(0, 0), Material.Empty).isEmpty())
        assertFalse(Tile(Position(0, 0), Material.Farm).isEmpty())
        assertFalse(Tile(Position(0, 0), Material.Forest).isEmpty())
        assertFalse(Tile(Position(0, 0), Material.Monster).isEmpty())
        assertFalse(Tile(Position(0, 0), Material.Mountain).isEmpty())
        assertFalse(Tile(Position(0, 0), Material.Village).isEmpty())
        assertFalse(Tile(Position(0, 0), Material.Water).isEmpty())
    }

}