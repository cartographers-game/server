package dev.vrba.cartographers.server

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CartographersServerApplication

fun main(args: Array<String>) {
    runApplication<CartographersServerApplication>(*args)
}
