package dev.vrba.cartographers.server.service

import dev.vrba.cartographers.server.domain.Player
import dev.vrba.cartographers.server.repository.PlayerRepository
import kotlinx.coroutines.reactor.awaitSingle
import kotlinx.coroutines.reactor.awaitSingleOrNull
import org.springframework.stereotype.Service
import java.nio.ByteBuffer
import java.security.SecureRandom
import java.util.*
import kotlin.random.Random

@Service
class PlayerService(private val repository: PlayerRepository) {

    suspend fun createPlayer(username: String): Player {
        val token = generateRandomToken()
        val player = Player(id = UUID.randomUUID(), username = username, token = token)

        return repository.insert(player).awaitSingle()
    }

    suspend fun findPlayerByToken(token: String): Player? {
        return repository.findByToken(token).awaitSingleOrNull()
    }

    /**
     * Doesn't have to be cryptographically secure...
     * The only thing that matters is having a low collision rate
     */
    private fun generateRandomToken(): String {
        val pool = ('a' .. 'z') + ('A' .. 'Z')
        val seed = ByteBuffer.wrap(SecureRandom.getSeed(Long.SIZE_BYTES)).long
        val random = Random(seed)
        val sequence = generateSequence { pool.random(random) }

        return sequence.take(32).joinToString("")
    }
}