package dev.vrba.cartographers.server.engine

data class Tile(val position: Position, val material: Material) {

    fun isEmpty(): Boolean = this.material == Material.Empty

}
