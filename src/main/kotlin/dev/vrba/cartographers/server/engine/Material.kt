package dev.vrba.cartographers.server.engine

enum class Material {
    Empty,
    Mountain,
    Monster,
    Forest,
    Village,
    Farm,
    Water
}