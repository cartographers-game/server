package dev.vrba.cartographers.server.engine

data class Position(val x: Int, val y: Int) {

    // Return all adjacent positions including diagonally adjacent positions
    fun neighbours(): Set<Position> {
        val vectors = listOf(
            Position(-1, -1),
            Position(-1, 0),
            Position(-1, 1),
            Position(0, -1),
            Position(0, 1),
            Position(1, -1),
            Position(1, 0),
            Position(1, 1),
        )

        return vectors.map { this + it }.toSet()
    }

    operator fun plus(other: Position): Position = Position(x + other.x, y + other.y)

}