package dev.vrba.cartographers.server.repository

import dev.vrba.cartographers.server.domain.Player
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono
import java.util.UUID

@Repository
interface PlayerRepository : ReactiveMongoRepository<Player, UUID> {

    fun findByToken(token: String): Mono<Player>

}