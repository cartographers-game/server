package dev.vrba.cartographers.server.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.util.UUID

@Document(collection = "players")
data class Player(
    @Id
    val id: UUID,
    val token: String,
    val username: String
)
